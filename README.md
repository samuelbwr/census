# Census App with Spring Boot and React Native
Simple App to show some statistics about the included database.

### To run them individually

- Back-end

```bash
mvn spring-boot:run 
```

- Front-end

```bash
yarn install

yarn start
```



After this, just follow the instructions on the terminal.



#### Things to be done

- Pagination
- Cache
- More tests on the front-end
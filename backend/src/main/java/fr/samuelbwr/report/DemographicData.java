package fr.samuelbwr.report;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class DemographicData {
    @Column
    @Id
    private String value;

    @Column
    private Long count;

    @Column
    private Double ageAverage;


}

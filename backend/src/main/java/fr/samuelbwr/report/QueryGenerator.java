package fr.samuelbwr.report;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

@Component
public class QueryGenerator {

    String generateForDemographicData( @Param( "categoryName" ) String categoryName ) {
        return "select `" + categoryName + "` as value, count(`" + categoryName + "`) as count, ROUND(avg(age),1) as age_average " +
                "from census_learn_sql where value is not null group by `" + categoryName+ "` order by 2 desc";
    }
}

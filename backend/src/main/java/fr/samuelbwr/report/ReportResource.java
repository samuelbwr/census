package fr.samuelbwr.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( "api/reports" )
public class ReportResource {

    @Autowired
    private DemographicReportRepository demographicReportRepository;

    @GetMapping( "demographic" )
    public List<DemographicData> getDemographicReport( @RequestParam( "category" ) String categoryName ) {
        return demographicReportRepository.retrieveDemographicDataForCategory( categoryName, 0, 100 );
    }
}

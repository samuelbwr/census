package fr.samuelbwr.report;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class DemographicReportRepository {

    @Autowired
    private EntityManager em;

    @Autowired
    private QueryGenerator queryGenerator;

    List<DemographicData> retrieveDemographicDataForCategory( String categoryName, int offset, int limit ) {
        String query = queryGenerator.generateForDemographicData( categoryName );
        return em.createNativeQuery( query, DemographicData.class )
                .setMaxResults( limit ).setFirstResult( offset )
                .getResultList();
    }

}

package fr.samuelbwr.categories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@org.springframework.stereotype.Repository
public interface CategoryRepository extends Repository<Category, Long> {

    @Query(value = "PRAGMA table_info(census_learn_sql)", nativeQuery = true)
    @Transactional(readOnly = true)
    List<Category> findAvailableCategories();


}

package fr.samuelbwr.categories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/categories")
public class CategoryResource {

    @Autowired
    private CategoryRepository repository;

    @GetMapping
    public List<Category> getCategories(){
        List<Category> categories = repository.findAvailableCategories();
        removeAge(categories);
        return categories;
    }

    private void removeAge( List<Category> categories ) {
        categories.remove( 0 );
    }
}

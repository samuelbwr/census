package fr.samuelbwr.categories;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Category {
    @Column(name = "cid")
    @Id
    private Long id;

    @Column(name = "name")
    private String name;

}

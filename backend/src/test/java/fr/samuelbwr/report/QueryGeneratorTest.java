package fr.samuelbwr.report;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith( SpringRunner.class )
@SpringBootTest
public class QueryGeneratorTest {

    private static final String QUERY = "select `education` as value, count(`education`) as count, ROUND(avg(age),1) as age_average " +
            "from census_learn_sql where value is not null group by `education` order by 2 desc";

    @Autowired
    private QueryGenerator queryGenerator;

    @Test
    public void ensureWillCreateCorrectlyTheQuery() {
        String createdQuery = queryGenerator.generateForDemographicData( "education" );
        Assert.assertEquals( QUERY, createdQuery );
    }
}

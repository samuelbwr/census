import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';

import Categories from './src/scenes/Categories';
import DemographicReport from './src/scenes/Reports/Demographic';

export default class App extends Component {
  render() {  
    return (
      <Router> 
        <Scene key="root">
          <Scene key="categoryList" component={Categories} hideNavBar={true} initial={true} />
          <Scene key="demographicReport" component={DemographicReport}/>
        </Scene> 
      </Router>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

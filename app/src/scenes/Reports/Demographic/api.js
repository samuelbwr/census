import apiConfig from '../../../services/api/config';

const endPoints = {
	fetchDemographicReport: '/api/reports/demographic?category='
};

export const fetchDemographicReport = (categoryName) => {
	return fetch(apiConfig.url+endPoints.fetchDemographicReport+encodeURIComponent(categoryName))
		.then(response => response.json()); 
} 
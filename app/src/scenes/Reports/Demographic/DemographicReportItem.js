import React from 'react';
import { TouchableOpacity, View, StyleSheet, Text } from 'react-native';
import DemographicData from './DemographicData';

export default class DemographicReportItem extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}> 
          <Text style={styles.title}> 
            {this.props.item.value}
          </Text>
          <DemographicData ageAverage={this.props.item.ageAverage}
                          count={this.props.item.count}
                          greaterCount={this.props.greaterCount}/>  
      </View>  
    );
  }
} 
 
const styles = StyleSheet.create({
  container: { 
    padding: 17,
    flex:1,
    flexDirection: 'row',
    marginBottom: 5, 
    marginTop: 10
  }, 
  title: {
    fontSize: 17,  
    marginLeft: 10, 
    fontWeight: '100',
    fontFamily: 'Roboto',
    zIndex: 2, 
    width: '80%',
  }
});

import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import * as reportsApi from './api'
import humanizeString from 'humanize-string';
import DemographicReportItem from './DemographicReportItem';
import Loading from '../../../components/Loading';

export default class DemographicReport extends React.Component {
  constructor(props) {
    super(props); 
    this.state = {
      loading: true,
      data: null
    }
  } 
 
  componentDidMount(){ 
    reportsApi.fetchDemographicReport(this.props.item.name)
      .then( data => {
        this.setState({
          data,
          loading: false
        })})
      .catch(error => console.log("Failed to connect to the server"));
  }

  keyExtractor = (item, index) => index+'';

  render() {
    if (this.state.loading)  
      return <Loading/>;
    
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          keyExtractor={this.keyExtractor}
          renderItem={({item}) => <DemographicReportItem greaterCount={this.state.data[0].count} item={item}/>}/>
      </View>
    );
  }
}
 
const styles = StyleSheet.create({ 
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});

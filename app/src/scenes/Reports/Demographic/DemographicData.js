import React from 'react';
import { TouchableOpacity, View, StyleSheet, Text } from 'react-native';
import DataBar from '../../../components/DataBar';

export default class DemographicData extends React.PureComponent {

  calculateAgeBoxWidth = (age) => (100*age)/120;

  calculateCitizenCountBoxWidth = (count) => (100*count)/this.props.greaterCount;

  render() {
    return (
      <View style={styles.container}>
        <DataBar barWidth={this.calculateAgeBoxWidth(this.props.ageAverage)+'%'}
                barColor="#d0d0d0">
          Average age{"\n"}<Text style={styles.bold}>{this.props.ageAverage}</Text> years
        </DataBar>
        <DataBar barWidth={this.calculateCitizenCountBoxWidth(this.props.count)+'%'}
                barColor="#eee">
          Citizens{"\n"}<Text style={styles.bold}>{this.props.count}</Text>
        </DataBar>
      </View>  
    );
  }
} 
 
const styles = StyleSheet.create({
  container:{
    flexDirection: 'column',
    flex:1,
    alignSelf: 'center',
    position: 'absolute',
    right: 4,
    width: '100%',
    zIndex: 1,
  },
  bold:{
    fontWeight: 'bold'
  }
});

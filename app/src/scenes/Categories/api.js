import apiConfig from '../../services/api/config';

const endPoints = {
	fetchAll: '/api/categories'
};

export const fetchAll = () => {
	return fetch(apiConfig.url+endPoints.fetchAll)
		.then(response => response.json()); 
}
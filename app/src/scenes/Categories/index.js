import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import * as categoriesApi from './api'
import CategoryItem from './CategoryItem';
import { Actions } from 'react-native-router-flux';
import humanizeString from 'humanize-string';
import LineSeparator from '../../components/LineSeparator';
import Loading from '../../components/Loading';

export default class Categories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      categories: []
    }
  }

  componentDidMount(){
    categoriesApi.fetchAll()
    .then( categories =>
      this.setState({
        categories,
        loading: false 
      }))
    .catch(error => console.log("Failed to connect to the server"));
  }

  handleItemPress(item){
    Actions.demographicReport({item, title: humanizeString(item.name)})
  }

  keyExtractor = (item) => item.id+'';

  render() {
    if (this.state.loading)  
      return <Loading/>;
    
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.categories}
          keyExtractor={this.keyExtractor}
          ItemSeparatorComponent={() => <LineSeparator/>}
          renderItem={({item}) => <CategoryItem item={item} onItemPress={this.handleItemPress}/>}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff', 
    paddingTop: 24
  },
  separator: {
    height: 1,
    width: "84%",
    backgroundColor: "#DDD",
    marginLeft: "8%"
  }
});

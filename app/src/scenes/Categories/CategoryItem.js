import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import FormattedText from '../../components/FormattedText';

export default class CategoryItem extends React.PureComponent {
  onPress = () => {
    this.props.onItemPress(this.props.item);
  };

  render() {
    return (
      <TouchableOpacity onPress={this.onPress} style={styles.container}> 
          <FormattedText style={styles.title}> 
            {this.props.item.name}
          </FormattedText>      
      </TouchableOpacity>
    );
  }
}
 
const styles = StyleSheet.create({
  container: { 
    padding: 17,
    flex:1,
  },
  title: {
    fontSize: 18,
    marginLeft: 30,
    fontWeight: '100',
    fontFamily: 'Roboto' 
  }
});

import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

export default class DataBar extends React.PureComponent {

  getCustomStyle = (width, backgroundColor) => {
    return {width, backgroundColor}
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.bar, this.getCustomStyle(this.props.barWidth,this.props.barColor )]}/>  
        <Text style={styles.text}>{this.props.children}</Text>
      </View> 
    );
  }
} 
 
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  bar:{
    right: 0,    
    height: 32,
  },
  text: {  
    fontSize: 11,
    height: 32, 
    width: '20%',
    textAlign: 'right',
    fontWeight: '100', 
    fontFamily: 'Roboto',
    paddingTop: 2
  }
});

import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

export default class Loading extends React.PureComponent {

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Loading..</Text>
      </View> 
    );
  }
} 
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',  
    alignItems: 'center'
  },
  text: {
    fontSize: 16, 
    fontWeight: '100', 
    fontFamily: 'Roboto',
  },
  bold:{
    fontWeight: 'bold'
  }
});

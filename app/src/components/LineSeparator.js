import React from 'react';
import { StyleSheet, View } from 'react-native';

export default LineSeparator = () => {
  return (
    <View style={styles.separator}/>
  );
};	

const styles = StyleSheet.create({ 
  separator: {
    height: 1,
    width: "84%",
    backgroundColor: "#DDD",
    marginLeft: "8%"
  }
});
import React from 'react';
import { Text } from 'react-native';
import humanizeString from 'humanize-string';

const formatText = (text) => humanizeString( text );

export default FormattedText = (props) => 
	<Text {...props}>{formatText(props.children)}</Text>

import React from 'react';
import Demographic from '../../../../src/scenes/Reports/Demographic';

import renderer from 'react-test-renderer';

it('renders demographic reports without crashing', () => {
  const rendered = renderer.create(<Demographic item={{name: 'education', id: 1}}/>).toJSON();
  expect(rendered).toMatchSnapshot();
});


import React from 'react';
import Categories from '../../../src/scenes/Categories';

import renderer from 'react-test-renderer';

it('renders categories without crashing', () => {
  const rendered = renderer.create(<Categories />).toJSON();
  expect(rendered).toMatchSnapshot();
});

